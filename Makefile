CC=gcc
CCOPTS=--std=gnu99 -D_LIST_DEBUG_
AR=ar 

OBJS=linked_list.o\
     fakeprocess_rw.o\
     fake_os.o 

HEADERSS=linked_list.h fakeprocess_rw.h 

BINS= test_fakeprocess scheduler_sim

.phony: clean all

all:    $(BINS)

%.o:    %.c $(HEADERS)
	 $(CC)  $(CCOPTS) -c -o  $@ $<

test_fakeprocess:  test_fakeprocess.c $(OBJS)
	$(CC)  $(CCOPTS)  -o $@ $^

scheduler_sim:  scheduler_sim.c $(OBJS)
	 $(CC)  $(CCOPTS)  -o $@ $^

clean:
	rm -rf *.o *~ $(OBJS) $(BINS)
