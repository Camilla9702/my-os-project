# My OS project

Questo progetto vede la realizzazione di un simulatore di scheduler, un programma facente parte di un OS, che prende in esame un insieme di richieste da parte di processi per accedere ad una risorsa e solo coloro che rispettano una cosiddetta politica di scheduler potranno accedervi per primi. Si crea cosi una coda di processi e con essa una coda di priorità che andrà a decretare il primo processo che può accedere alla risorsa richiesta.
Nel caso in cui ci sia una sola CPU, i processi che si trovano nella loro coda di ready aspirano ad ottenere l'accesso ad essa e di mantenerla per almeno tutto il tempo richiesto. Questo non sempre è possibile e tutto dipende dallo scheduler che può essere di due tipi:

- SCHEDULER PREEMPTIVE: con il "diritto d prelazione", ovvero lo scheduler ha il potere di togliere il processore ad un processo anche se esso potrebbe continuare la propria esecuzione.

- SCHEDULER NON-PREEMPTIVE: lo scheduler deve attendere che il processo termini la sua esecuzione o che passi dallo stato di esecuzione allo stato di wait.

La CPU (central processing unit) è il cuore del calcolatore ed è colei che prende i dati, li manipola, li elabora e li carica in memoria, aiutata dalla CU (control unit), dall' ALU (arithmetic-logic unit) e dai vari registri.

_Ma cosa accade se anzichè una CPU, fossero presenti due, tre o quattro CPU?_

Si parla in tal caso di processori multi-core. Essi difatti furono introdotti per aumentare le performance e per questo i produttori decisero di aumentare il numero di core per ogni singola CPU. Ad esempio una CPU dual core ha due unità di elaborazione ed è vista dal sistema operativo come una doppia CPU.

A tal proposito il simulatore di scheduler dovrà decidere non solo quale processo, tra quelli nella coda di ready, accederà alla CPU ma soprattutto a quale CPU sarà destinato.

Nel mio caso, ogni processo terrà conto di tre CPU.
Se il numero dei processi che arrivano è superiore rispetto al numero delle CPU presenti allora i primi tre processi andranno nella coda di ready, la coda principale dei processi, i restanti verranno inseriti in una coda secondaria che ha il solo scopo di mantenere tali processi, i quali, non appena si libererà una CPU, verranno spostati nella coda di ready.

Dopodiché viene fatto un altro confronto, se il numero dei processi che si trovano della coda di ready è minore o uguale al numero delle CPU, allora banalmente ad ogni processo verrà assegnata una CPU e continuerà la sua esecuzione su di essa secondo l'algoritmo di scheduler implementato. Quest'ultimo è l'algoritmo di Round Robin in cui i processi verranno eseguiti in maniera circolare ed ognuno dovrà confrontarsi con il quanto di tempo imposto dall'algoritmo. Se il tempo richiesto dal processo nella CPU è minore del quanto allora occuperà la sua CPU per tutto il tempo altrimenti verrà spostato nella coda di wait ed eseguito il prossimo processo, per poi riprendere con il tempo rimanente non appena sarà possibile.
Nel caso in cui, però, i processi sono di più rispetto alle CPU disponibili (nel mio progetto sono 5), non appena uno dei tre processi termina viene liberata la CPU sulla quale stava lavorando e viene inserito nella coda di ready il primo processo in cima alla coda di ready2.
Il numero della CPU che si è liberata viene salvato in modo tale che il processo appena entrato nella coda di ready possa usufruire di essa e non di altre che magari sono ancora in esecuzione. Importante ricordare che non possono essere eseguiti contemporaneamente due processi nella stessa CPU.

