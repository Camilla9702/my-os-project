#pragma
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "fake_os.h"


// Inizializza la struttura di FakeOS
void FakeOS_init(FakeOS* os) {
    for (int i=0; i < NUMCPU; i++) {
        os->running[i] = (FakePCB*)malloc(sizeof(FakePCB));
        os->running[i] = 0;
    }
    List_init(&os->waiting);
    List_init(&os->ready);
    List_init(&os->processes);
    os->timer=0;
    os->schedule_fn=0;
}


// Arriva il primo processo da dare in pasto al SO
void FakeOS_createProcess(FakeOS* os, FakeProcess* p) {
    assert(p->arrival_time==os->timer && "time mismatch in creation");

    // controllo che nell'elenco dei PCB non ci sia un PCB con lo stesso pid,
    // dal momento che devono essere unici

    ListItem* aux=os->ready.first;
    while(aux){

        FakePCB* pcb=(FakePCB*)aux;
        assert(pcb->pid!=p->pid && "pid taken");
        aux=aux->next;
    }

    aux=os->waiting.first;
    while(aux){

        FakePCB* pcb=(FakePCB*)aux;
        assert(pcb->pid!=p->pid && "pid taken");
        aux=aux->next;
    }

    // arrivati a questo punto, dato che non esiste alcun PCB,
    // ne viene creato uno nuovo
    FakePCB* new_pcb=(FakePCB*) malloc(sizeof(FakePCB));
    new_pcb->list.next=new_pcb->list.prev=0;
    new_pcb->pid=p->pid;
    new_pcb->events=p->events;

    assert(new_pcb->events.first && "process without events");


    // In base al tipo del primo evento, questo verrà inserito nella
    // coda di ready o nella coda di wait
    ProcessEvent* e=(ProcessEvent*)new_pcb->events.first;
    switch(e->type){
    case CPU:
        List_pushBack(&os->ready, (ListItem*) new_pcb);
        break;
    case IO:
        List_pushBack(&os->waiting, (ListItem*) new_pcb);
        break;
    default:
        assert(0 && "illegal resource");
        ;
    }

}

// il SO inizia a fare il suo dovere
void FakeOS_simStep(FakeOS* os, int numCpu){
    printf("************** TIME: %05d **************\n", os->timer);

    // esamina il processo in attesa di essere avviato
    // e crea tutti i processi a partire da ora


    ListItem* aux=os->processes.first;
    while (aux){
        FakeProcess* proc=(FakeProcess*)aux;
        FakeProcess* new_process=0;
        if (proc->arrival_time==os->timer){
            new_process=proc;
        }
        aux=aux->next;
        if (new_process) {
            printf("\tcreate pid: %d\n", new_process->pid);
            new_process=(FakeProcess*)List_detach(&os->processes, (ListItem*)new_process);
            FakeOS_createProcess(os, new_process);
            free(new_process);
        }
    }

    FakePCB* running[NUMCPU];

    for (int i=0; i < NUMCPU; i++) {
        running[i] = (FakePCB*)malloc(sizeof(FakePCB));
        running[i]->pid = -1;
    }

    int runn=1;
    int wa=1;

    // esamina la waiting list e mette in ready tutti quei processi che hanno
    // terminato il loro burst CPU
    aux=os->waiting.first;
    while(aux) {

        FakePCB* pcb = (FakePCB*)aux;
        aux=aux->next;
        ProcessEvent* e=(ProcessEvent*) pcb->events.first;
        printf("\twaiting pid: %d\n", pcb->pid);
        assert(e->type==IO);
        e->duration--; // sottraggo 1 per ogni evento della lista di waiting
        printf("\t\tremaining time: %d\n",e->duration);

        if (e->duration==0){ // quando è finito un burst di I/O
            printf("\t\tend burst\n");
            List_popFront(&pcb->events);
            free(e);
            List_detach(&os->waiting, (ListItem*)pcb);
            if (! pcb->events.first) { // se è finito il processo, lo uccidi
                printf("\t\tend process\n");
                free(pcb);
            } else { //gestisce il prossimo evento
                e=(ProcessEvent*) pcb->events.first;
                switch (e->type){
                case CPU:
                    printf("\t\tmove to ready\n");
                    List_pushBack(&os->ready, (ListItem*) pcb);
                    wa = -1;
                    break;
                case IO:
                    printf("\t\tmove to waiting\n");
                    List_pushBack(&os->waiting, (ListItem*) pcb);
                    break;
                }
            }
        }
    }


    printf("Running process and %d CPU avaible\n", NUMCPU);
    for (int c=1; c <= NUMCPU; c++)  {

        if (os->running[c-1] == 0) {
            runn = -1;
            continue;
        }

        running[c-1] = os->running[c-1];

        if (running[c-1]->pid > 0) {

            printf("\trunning pid: %d", running[c-1]->pid);
            printf(" on the %d°CPU\n", c);

            ProcessEvent* e = (ProcessEvent*) running[c-1]->events.first;
            assert(e->type==CPU);
            e->duration--;
            printf("\t\tremaining time: %d\n",e->duration);
            if (e->duration == 0){
                printf("\t\tend burst\n");
                List_popFront(&running[c-1]->events);
                free(e);
                if (! running[c-1]->events.first) { // se non ho più eventi per questo processo
                    printf("\t\t\tend process, %d°CPU free\n",c);
                    if (os->ready.size > 0) {
                        printf("\t\t\tenter the next process, if any\n");
                        List_pushBack(&os->ready, (ListItem*)List_popFront(&os->ready));
                        runn = -1;
                    }
                    os->finito++; //finiti tutti i processi
                } else {
                    e = (ProcessEvent*) running[c-1]->events.first;
                    switch (e->type){
                    case CPU:
                        List_pushBack(&os->ready, (ListItem*)running[c-1]);
                        if (os->ready.size > 0) {
                            printf("\t\tquantum finished for this process on this CPU,\n");
                            printf("\t\tmove to ready another process, if any\n");
                            runn = -1;
                        }
                        break;
                    case IO:
                        printf("\t\tmove to waiting\n");
                        List_pushBack(&os->waiting, (ListItem*) running[c-1]);
                        runn = -1;
                        break;
                    }
                }
                os->running[c-1] = 0;
            }
        }
    }


    // se viene chiamata la funzione schedule e quindi se è definita
    // si comporta da schedule e decide quale processo accede alla CPU.
    if (os->schedule_fn && (runn < 0 || wa < 0)) {
        (*os->schedule_fn)(os, os->schedule_args);
    }

    ++os->timer;
}
