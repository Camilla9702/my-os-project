#include "fakeprocess_rw.h"
#include "linked_list.h"
#pragma once

#define NUMCPU 4

// FakePCB, associato ad ogni processo rappresentato da una lista

typedef struct {
    ListItem list;
    int pid;
    ListHead events;
} FakePCB;

// ScheduleFn è una funzione che in realtà non bisogna implementare ed
// entra in gioco quando principalmente un processo finisce il suo quanto
// di tempo oppure arriva una I/O

struct FakeOS;
typedef void (*ScheduleFn)(struct FakeOS* os, void* args);

// FakeOS che come un OS ha una lista di processi, di processi di ready
// e waiting e lo stato di un processo

typedef struct FakeOS{
    FakePCB* running[NUMCPU];
    int finito;
    ListHead ready;
    ListHead waiting;
    int timer;
    ScheduleFn schedule_fn;
    void* schedule_args;
    ListHead processes;
} FakeOS;

void FakeOS_init(FakeOS* os);
void FakeOS_simStep(FakeOS* os,int numCpu);

