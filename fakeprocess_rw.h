#pragma once
#include "linked_list.h"

// ResourceType, indica il tipo di risorsa per il processo
typedef enum {CPU=0, IO=1} ResourceType;

// ProcessEvent, evento di un processo rappresentato da una lista
typedef struct {
    ListItem list;
    ResourceType type;
    int duration;
} ProcessEvent;

// FakeProcess, processo rappresentato da una lista
typedef struct {
    ListItem list;
    int pid;
    int arrival_time;
    ListHead events;
} FakeProcess;

int FakeProcess_read(FakeProcess* p, const char* filename);
int FakeProcess_write(const FakeProcess* p, const char* filename);
