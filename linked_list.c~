#include "linked_list.h"
#include <assert.h>

void List_init(ListHead* head) {
  head->first=0;
  head->last=0;
  head->size=0;
}

// trovare un oggetto della lista
ListItem* List_find(ListHead* head, ListItem* item) {
  ListItem* aux=head->first;
  while(aux){
    if (aux==item)
      return item;
    aux=aux->next;
  }
  return 0;
}

// inserire un oggetto nella lista
ListItem* List_insert(ListHead* head, ListItem* prev, ListItem* item) {
  if (item->next || item->prev)
    return 0;
  
  // accertarsi che l'elemento corrente non sia già nella lista
  // ma che ci sia il precedente
#ifdef _LIST_DEBUG_
  ListItem* instance=List_find(head, item);
  assert(!instance);

  if (prev) {
    ListItem* prev_instance=List_find(head, prev);
    assert(prev_instance);
  }
#endif

  ListItem* next= prev ? prev->next : head->first;
  if (prev) {
    item->prev=prev;
    prev->next=item;
  }
  if (next) {
    item->next=next;
    next->prev=item;
  }
  if (!prev)
    head->first=item;
  if(!next)
    head->last=item;
  ++head->size;
  return item;
}

// rimuovere un elemento dalla lista
ListItem* List_detach(ListHead* head, ListItem* item) {

  // verificare che l'elemento sia nella lista
#ifdef _LIST_DEBUG_
  ListItem* instance=List_find(head, item);
  assert(instance);
#endif

  ListItem* prev=item->prev;
  ListItem* next=item->next;
  if (prev){
    prev->next=next;
  }
  if(next){
    next->prev=prev;
  }
  if (item==head->first)
    head->first=next;
  if (item==head->last)
    head->last=prev;
  head->size--;
  item->next=item->prev=0;
  return item;
}

// inserire un elemento in coda
ListItem* List_pushBack(ListHead* head, ListItem* item) {
  return List_insert(head, head->last, item);
};

// inserire un elemento in testa
ListItem* List_pushFront(ListHead* head, ListItem* item) {
  return List_insert(head, 0, item);
};

// rimuovere un elemento in testa
ListItem* List_popFront(ListHead* head) {
  return List_detach(head, head->first);
}