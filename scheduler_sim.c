#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "fake_os.h"

FakeOS os;
int numProc = 0;

// quanto di tempo per l'algoritmo RR
typedef struct {
    int quantum;
} SchedRRArgs;

// Algoritmo di scheduling: Round Robin

void schedRR(FakeOS* os, void* args_){
    SchedRRArgs* args=(SchedRRArgs*)args_;


    FakePCB* pcb[NUMCPU];
    for (int j=0; j < NUMCPU; j++) {
        pcb[j] = (FakePCB*)malloc(sizeof(FakePCB));
    }

    for (int i=0; i < NUMCPU; i++) {

        if (os->running[i] == 0 && os->ready.size>0) {
            pcb[i]=(FakePCB*) List_popFront(&os->ready);
            os->running[i] = pcb[i];
            assert(pcb[i]->events.first);

            ProcessEvent* e = (ProcessEvent*)pcb[i]->events.first;
            assert(e->type==CPU);

            // se la durata del burst CPU del processo è maggiore di quella del quanto
            // lo divido, dopo questo poi faccio partire un altro evento di processo
            // che ha la durata del suo burst meno quella del quanto corrente
            if (e->duration > args->quantum) {
                ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
                qe->list.prev=qe->list.next=0;
                qe->duration=args->quantum;
                e->duration-=args->quantum;
                qe->type=CPU;
                List_pushFront(&pcb[i]->events, (ListItem*)qe);
            };
        } else continue;
    }

}

int main(int argc, char** argv) {
    FakeOS_init(&os);
    SchedRRArgs srr_args;
    srr_args.quantum = 6;
    os.schedule_args=&srr_args;
    os.schedule_fn=schedRR;

    for (int i=1; i<argc; ++i){
        FakeProcess new_process;
        int num_events=FakeProcess_read(&new_process, argv[i]);
        printf("Process %d: {from->[%s], pid: %d, events: %d}\n",
            i, argv[i], new_process.pid, num_events);
        if (num_events) {
            FakeProcess* new_process_ptr=(FakeProcess*)malloc(sizeof(FakeProcess));
            *new_process_ptr=new_process;
            List_pushBack(&os.processes, (ListItem*)new_process_ptr);
        }
    }

    printf("\nNumber of processes in queue: %d\n", os.processes.size);

    if (os.processes.size > 0) numProc = os.processes.size;


    while(os.ready.first || os.waiting.first || os.processes.first || os.running) {
        if (os.finito == numProc) {
            break;
        }
        FakeOS_simStep(&os,NUMCPU);
    }

}
